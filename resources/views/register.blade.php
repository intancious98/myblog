<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Register</title>
</head>
<body>
    <nav>
        <a href="/">Home</a> |
        <a href="/register">Register</a> |
   </nav>


    <form action="kirim" method="POST">
        @csrf
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up From</h2>
    <p>First name :</p>
    <input type="text" name="fn">
    <p>Last name :</p>
    <input type="text" name="ln">
    <p>Gender :</p>
    <input type="radio" name ="gender" value="1">Male<br>
    <input type="radio" name ="gender" value="2" checked/>Female<br>
    <input type="radio" name ="gender"value="3">Other<br>
    <p>Nationality :</p>
    <select name="nationality" >
    <option value="1">Indonesia</option>
    <option value="2">Australia</option>
    <option value="3">Korea</option>
    </select>
    <p>Language Spoken :</p>
    <input type="checkbox" value="Bahasa Indonesia">
    <label for="Bahasa Indonesia">Bahasa Indonesia</label><br>
    <input type="checkbox" value="English">
    <label for="English">English</label><br>
    <input type="checkbox" value="Other">
    <label for="Other">Other</label><br>
    <p>Bio :</p>
    <textarea name="bio"  cols="30" rows="10"></textarea>
    <br>
    <input type="submit" value="Sign Up">
</form>
</body>
</html>