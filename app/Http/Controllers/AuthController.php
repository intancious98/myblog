<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function form()
    {
        return view('register');
    }
    public function kirim(Request $request)
    {
        // dd($request->all());
        $namadepan = $request['fn'];
        $namabelakang = $request['ln'];
        return view('welcome', compact('namadepan', 'namabelakang'));
    }
}
